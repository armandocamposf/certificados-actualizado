<?php
namespace App\Http\Controllers;

use App\Models\Matricula;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('matriculas/alumno/{alumno}', [MatriculaController::class, 'dataAlumno']);

Route::get('matriculas/data/general/{alumno}', [MatriculaController::class, 'dataGeneral']);
