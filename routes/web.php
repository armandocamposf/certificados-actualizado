<?php

use App\Http\Controllers\AlumnoController;
use App\Http\Controllers\AreaController;
use App\Http\Controllers\CursoController;
use App\Http\Controllers\DocenteController;
use App\Http\Controllers\MatriculaController;
use App\Http\Controllers\TipoDocumentoController;
use App\Http\Controllers\UserController;
use App\Models\Docente;
use App\Models\TipoDocumento;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Auth::routes();
Route::get('users-logout', [UserController::class, 'logout'])->name('users.logout');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('cursos', [CursoController::class, 'index'])->name('cursos.index');
Route::get('cursos/create', [CursoController::class, 'create'])->name('cursos.agregar');
Route::get('cursos/edit/{curso}', [CursoController::class, 'edit'])->name('cursos.edit');
Route::post('cursos/update/{curso}', [CursoController::class, 'update'])->name('cursos.update');
Route::post('cursos/store', [CursoController::class, 'store'])->name('cursos.store');
Route::get('cursos/activar/{curso}', [CursoController::class, 'activar'])->name('cursos.activar');
Route::get('cursos/desactivar/{curso}', [CursoController::class, 'desactivar'])->name('cursos.desactivar');
Route::get('cursos/cargaImagen/{curso}', [CursoController::class, 'carga'])->name('cursos.cargaImagen');
Route::post('cursos/uploadImage/{curso}', [CursoController::class, 'upload'])->name('cursos.uploadImage');


Route::get('alumnos', [AlumnoController::class, 'index'])->name('alumnos.index');
Route::get('alumnos/create', [AlumnoController::class, 'create'])->name('alumnos.agregar');
Route::post('alumnos/store', [AlumnoController::class, 'store'])->name('alumnos.store');
Route::get('alumnos/activar/{alumno}', [AlumnoController::class, 'activar'])->name('alumnos.activar');
Route::get('alumnos/desactivar/{alumno}', [AlumnoController::class, 'desactivar'])->name('alumnos.desactivar');
Route::get('alumnos/editar/{alumno}', [AlumnoController::class, 'edit'])->name('alumnos.edit');
Route::post('alumnos/update/{alumno}', [AlumnoController::class, 'update'])->name('alumnos.update');


Route::get('docentes', [DocenteController::class, 'index'])->name('docentes.index');
Route::get('docentes/create', [DocenteController::class, 'create'])->name('docentes.agregar');
Route::post('docentes/store', [DocenteController::class, 'store'])->name('docentes.store');
Route::get('docentes/editar/{docente}', [DocenteController::class, 'edit'])->name('docentes.edit');
Route::post('docentes/update/{docente}', [DocenteController::class, 'update'])->name('docentes.update');
Route::get('docentes/activar/{docente}', [DocenteController::class, 'activar'])->name('docentes.activar');
Route::get('docentes/desactivar/{docente}', [DocenteController::class, 'desactivar'])->name('docentes.desactivar');
Route::get('docentes/cargarFirma/{docente}', [DocenteController::class, 'firma'])->name('docentes.cargarFirma');
Route::post('docentes/uploadFirma/{docente}', [DocenteController::class, 'uploadImage'])->name('docentes.uploadImage');

Route::get('matriculas', [MatriculaController::class, 'index'])->name('matriculas.index');
Route::get('matriculas/nueva', [MatriculaController::class, 'create'])->name('matriculas.create');
Route::post('matriculas/nueva', [MatriculaController::class, 'store'])->name('matriculas.store');
Route::get('matriculas/eliminar/{matricula}', [MatriculaController::class, 'destroy'])->name('matriculas.destroy');
Route::get('matriculas/byAlumno', [MatriculaController::class, 'byAlumno'])->name('matriculas.alumno');


Route::get('verificador', [MatriculaController::class, 'vericador'])->name('verificador');
Route::post('certificado/', [MatriculaController::class, 'mostrar'])->name('mostrar-certificado');
Route::get('ver-certificado/{matricula}', [MatriculaController::class, 'mostrar2'])->name('ver-certificado');
Route::get('descargar-certificado/{matricula}', [MatriculaController::class, 'descargar'])->name('descargar-certificado');


Route::get('generar-qr/{matricula}', [MatriculaController::class, 'createQr'])->name('generarQr');
Route::get('/certificado/emision/pdf/{matricula}', [MatriculaController::class, 'emitirCertificado'])->name('emitir-certificado');
