<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lineas_certificados', function (Blueprint $table) {
            $table->id();
            $table->integer('idCurso');
            $table->string('titulo')->default('CERTIFICADO');
            $table->string('otorgado')->default('Otorgado a:');
            $table->string('descrip1')->default('Por Haber asistido al curso de:');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lineas_certificados');
    }
};
