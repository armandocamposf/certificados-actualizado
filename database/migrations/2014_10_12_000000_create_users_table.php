<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('idTipoDocumento')->nullable();
            $table->string('documento')->nullable();
            $table->string('name')->nullable();
            $table->string('lastName')->nullable();
            $table->string('email')->unique();
            $table->string('password')->nullable();
            $table->string('movil')->nullable();
            $table->integer('status')->default(1);
            $table->integer('typeUser')->default(1);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
