@extends('layouts.app')

@section('contenido')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12"></div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 mb-3">
                <a href="{{ route('cursos.index') }}" class="btn btn-warning w-100"> <i class="fa-solid fa-backward"></i> Regresar a Cursos</a>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header bg-success text-white">Cragar Formato del certificado del curso: {{ $curso->titulo }}</div>

                    <div class="card-body">
                        <form action="{{ route('cursos.uploadImage', $curso->id) }}" method="post" enctype="multipart/form-data">
                            @csrf
                        <div class="row">
                            <label class="col-sm-2 form-control-label mt-4">Fondo Certificado: <span class="tx-danger">*</span></label>
                            <div class="col-sm-10 mg-t-10 mg-sm-t-0 mt-3">
                              <input type="file" class="form-control" required name="fondo" >
                            </div>


                            <div class="col-sm-12 mt-3">
                                <button class="btn btn-success w-100 mt-3"> Guardar </button>
                            </div>
                          </div><!-- row -->

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

