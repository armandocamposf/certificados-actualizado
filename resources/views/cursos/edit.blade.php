@extends('layouts.app')

@section('contenido')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12"></div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 mb-3">
                <a href="{{ route('cursos.index') }}" class="btn btn-warning w-100"> <i class="fa-solid fa-backward"></i> Regresar a Cursos</a>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header bg-success text-white">Editar Curso</div>

                    <div class="card-body">
                        <form action="{{ route('cursos.update', $curso->id) }}" method="post">
                            @csrf
                        <div class="row">
                            <label class="col-sm-2 form-control-label">Titulo: <span class="tx-danger">*</span></label>
                            <div class="col-sm-10 mg-t-10 mg-sm-t-0 mt-3">
                              <input type="text" class="form-control" required name="titulo" value="{{ $curso->titulo }}">
                            </div>

                            <label class="col-sm-2 form-control-label">Descripcion: <span class="tx-danger">*</span></label>
                            <div class="col-sm-10 mg-t-10 mg-sm-t-0 mt-3">
                                <textarea  name="descripcion" id="editor1" rows="5" class="form-control ckeditor">{{ $curso->descripcion }}</textarea>
                            </div>


                            <label class="col-sm-4 form-control-label mt-3">Tipo de Duracion: <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0 mt-3">
                              <select name="tipo_duracion" class="form-control" id="">
                                <option value="">SELECCIONE</option>
                                <option value="1" @if($curso->typeDuracion == 1) selected @endif>Horas Pedagogicas</option>
                                <option value="2" @if($curso->typeDuracion == 2) selected @endif>Horas Academicas</option>
                              </select>
                            </div>

                            <label class="col-sm-4 form-control-label mt-3">Duracion (En Numeros): <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0 mt-3">
                              <input type="text" class="form-control" required name="duracion" value="{{ $curso->duracion }}">
                            </div>

                            <label class="col-sm-4 form-control-label mt-3">Fecha Emision: <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0 mt-3">
                              <input type="date" class="form-control" required name="fechaEmision" value="{{ $curso->fechaEmision }}">
                            </div>

                            <label class="col-sm-4 form-control-label mt-3">Fecha Inicio: <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0 mt-3">
                              <input type="date" class="form-control" required name="fechaInicio" value="{{ $curso->fechaInicio }}">
                            </div>

                            <label class="col-sm-4 form-control-label mt-3">Fecha Fin: <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0 mt-3">
                              <input type="date" class="form-control" required name="fechaFin" value="{{ $curso->fechaFin }}">
                            </div>

                            <label class="col-sm-4 form-control-label mt-3">Docente: <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0 mt-3">
                              <select name="idDocente" class="form-control js-example-basic-single1" id="">
                                @foreach ($docentes as $docente)
                                @if($docente->id == $curso->idDocente)
                                    <option value="{{ $docente->id }}" >{{ $docente->nombres }}</option>
                                    @endif
                                @endforeach
                                @foreach ($docentes as $docente)
                                @if($docente->id != $curso->idDocente)
                                    <option value="{{ $docente->id }}" >{{ $docente->nombres }}</option>
                                    @endif
                                @endforeach
                              </select>
                            </div>


                            <div class="col-sm-12 mt-3">
                                <button class="btn btn-success w-100 mt-3"> Guardar </button>
                            </div>
                          </div><!-- row -->

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('js')
<script>
$(document).ready(function() {
    $('.js-example-basic-single1').select2();
});
</script>
@endsection
