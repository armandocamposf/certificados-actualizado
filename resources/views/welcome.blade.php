<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <title>SSOMACAL | Buscador Certificados</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">

    <!-- Favicons -->
    <link href="icono.png" rel="icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700"
        rel="stylesheet">

    <!-- Bootstrap CSS File -->
    <link href="bootstrap.min.css" rel="stylesheet">

    <!-- Libraries CSS Files -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css"
        integrity="sha512-SzlrxWUlpfuzQ+pcUCosxcglQRNAq/DZjVsC0lE40xsADsfeQoEypE+enwcOiGjk/bSuGGKHEyjSoQ1zVisanQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="lightbox.min.css" rel="stylesheet">

    <link href="style.css" rel="stylesheet">
    <style>
        .size {
            width: 330px;
        }

        .size2 {
            width: 200px;
        }

        .azul{
            color: #001C7D!important;
        }

        .btn-azul{
            background-color: #001C7D!important;
        }

        .bg-azul{
            background-color: #001C7D!important;
            color: white;
        }

        .bg-mostaza{
            color: white;
            background-color: #BD9D12;
            border: solid 0px;
        }

        .bg-mostaza:hover{
            background-color: #CE9E1D;
            border: solid 0px;
        }
    </style>
</head>

<body>
    <img src="icono.png" style="width: 50%; margin-left:25%;" class="d-sm-none d-md-none d-lg-none " alt="">
    <header id="header d-none">
        <div class="container-fluid">

            <div id="logo" class="pull-left">
                <h1><a href="/" class="border-0">
                        <img  class="img-fluid d-none d-sm-block" src="icono.png" style="width: 170px;"> </a></h1>

            </div>

            <nav id="nav-menu-container" style="margin-top: -10rem;">
                <ul class="nav-menu">
                    <li class="menu-active"><a href="/verificador" class="azul">Verificador de Certificado</a></li>
                    <li class="menu-active"><a href="/" class="azul">Regresar</a></li>
                </ul>
            </nav>
        </div>
    </header>

    <section id="intro" id="intro" style="margin-top: -10rem;">
        <div class="intro-container">
            <div id="introCarousel" class="carousel  slide carousel-fade" data-ride="carousel">

                <div class="carousel-inner" role="listbox">

                    <div class="carousel-item active">
                        <div class="carousel-container" style="align-items: end; margin-top: 100px;">
                            <div class="carousel-content">
                                <h2>CERTIFICADOS</h2>
                                <p>Consulte a través del buscador con su número de DNI</p>

                                <div class="form-row justify-content-center" id="buscadornone">
                                    <div class="form-group col-10">
                                        <div class="input-group mb-3">
                                            <input type="text" id="doc" name="doc" class="form-control"
                                                id="doc" placeholder="Número de DNI" autocomplete="off">
                                            <div class="input-group-append">
                                                <button type="button" onclick="buscarData()" class="btn btn-azul text-white"
                                                    id="qrbuttom"><i class="fa fa-search"></i> Buscar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </section>

    <div class="container" style="display: none; margin-top: 3rem" id="dataAlumno">
        <button class="btn btn-danger w-100 mb-3 bg-mostaza" onclick="limpiar()">Regresar</button>

        <div class="alert alert-danger mt-3" role="alert" id="mensajeerror" style="display: none">
            This is a danger alert—check it out!
        </div>

        <div class="card mt-3" id="sinerror">
            <div class="card-header bg-azul text-white">
                Datos del Alumno
            </div>

            <div class="card-body">
                <div class="list-group mb-3">
                    <div class="list-group-item">
                        <div class="row">
                            <div class="col-sm-5">Documento</div>
                            <div class="col-sm-5" id="tipoDocumento">Tipo de Documento</div>
                        </div>
                    </div>
                    <div class="list-group-item">
                        <div class="row">
                            <div class="col-sm-5">Nombres</div>
                            <div class="col-sm-5" id="nombres"></div>
                        </div>
                    </div>
                    <div class="list-group-item">
                        <div class="row">
                            <div class="col-sm-5">Apellidos</div>
                            <div class="col-sm-5" id="apellidos"></div>
                        </div>
                    </div>
                    <div class="list-group-item">
                        <div class="row">
                            <div class="col-sm-5">Correo</div>
                            <div class="col-sm-5" id="correo"></div>
                        </div>
                    </div>
                    <div class="list-group-item">
                        <div class="row">
                            <div class="col-sm-5">Telefono</div>
                            <div class="col-sm-5" id="telefono"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card mt-3" id="sinerror2">
            <div class="card-header bg-azul text-white">
                Datos de las Matriculas y Certificados
            </div>

            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12 col-md-12 sol-xs-12">
                        <table class="table table-responsive table-striped table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>
                                        <div class="size"></div>
                                    </th>
                                    <th>
                                        <div class="size">Curso</div>
                                    </th>
                                    <th>
                                        <div class="size2">Fecha Inicio
                                    </th>
                                    <th>
                                        <div class="size2">Fecha Fin
                                    </th>
                                    <th>
                                        <div class="size2">Fecha Emision
                                    </th>
                                    <th><div class="size2"></div></th>
                                </tr>
                            </thead>
                            <tbody id="dataMatriculas">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
    <script src="https://code.jquery.com/jquery-3.6.3.min.js"
        integrity="sha256-pvPw+upLPUjgMXY0G+8O0xUf+/Im1MZjXxxgOcBQBXU=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/1.3.2/axios.min.js"
        integrity="sha512-NCiXRSV460cHD9ClGDrTbTaw0muWUBf/zB/yLzJavRsPNUl9ODkUVmUHsZtKu17XknhsGlmyVoJxLg/ZQQEeGA=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <script>
        function buscarData() {
            $("#intro").css('display', 'none')

            axios.get('/api/matriculas/data/general/' + $("#doc").val()).then((data) => {
                if (data.data.error == 404) {
                    $("#sinerror").css('display', 'none')
                    $("#mensajeerror").css('display', 'block')
                    $("#mensajeerror").html(data.data.message)
                } else {
                    const alumno = data.data.alumno

                    if (alumno.idTipoDocumento == 1) {
                        $("#tipoDocumento").html("DNI - " + alumno.documento)
                    } else {
                        $("#tipoDocumento").html("OTRO - " + alumno.documento)
                    }

                    $("#nombres").html(alumno.nombres)
                    $("#apellidos").html(alumno.apellidos)
                    $("#correo").html(alumno.correo)
                    $("#telefono").html(alumno.telefono)
                    $("#mensajeerror").css('display', 'none')
                    $("#sinerror").css('display', 'block')
                    $("#dataMatriculas").html(data.data.html)
                }

                $("#dataAlumno").css('display', 'block')
            })
        }

        function limpiar() {
            $("#doc").val("")
            $("#intro").css('display', 'block')
            $("#dataAlumno").css('display', 'none')
        }
    </script>

</body>

</html>
