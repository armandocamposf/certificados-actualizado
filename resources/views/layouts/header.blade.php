<ul class="nav am-sideleft-menu">
    <li class="nav-item">
      <a href="{{ route('home') }}" class="nav-link">
        <i class="icon ion-ios-home-outline"></i>
        <span>Incio</span>
      </a>
    </li><!-- nav-item -->
    <li class="nav-item">
      <a href="" class="nav-link with-sub">
        <i class="icon ion-ios-gear-outline"></i>
        <span>Mantenimientos</span>
      </a>
      <ul class="nav-sub">
        <li class="nav-item"><a href="{{ route('cursos.index') }}" class="nav-link">Cursos</a></li>
        <li class="nav-item"><a href="{{ route('alumnos.index') }}" class="nav-link">Alumnos</a></li>
        <li class="nav-item"><a href="{{ route('docentes.index') }}" class="nav-link">Docentes</a></li>
      </ul>
    </li><!-- nav-item -->

    <li class="nav-item">
        <a href="" class="nav-link with-sub">
          <i class="icon ion-ios-gear-outline"></i>
          <span>Matriculas</span>
        </a>
        <ul class="nav-sub">
          <li class="nav-item"><a href="{{ route('matriculas.create') }}" class="nav-link">Nueva Matricula</a></li>
          <li class="nav-item"><a href="{{ route('matriculas.index') }}" class="nav-link">Todas las Matriculas</a></li>
          <li class="nav-item"><a href="{{ route('matriculas.alumno') }}" class="nav-link">Matriculas Por Alumno</a></li>
          {{--  <li class="nav-item"><a href="{{ route('docentes.index') }}" class="nav-link">Matriculas por Curso</a></li>  --}}
        </ul>
      </li><!-- nav-item -->
  </ul>
