@extends('layouts.app')

@section('contenido')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12"></div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 mb-3">
                <a href="{{ route('alumnos.agregar') }}" class="btn btn-success w-100"> <i class="fa fa-plus-circle"></i>
                    Agregar Nuevo Alumno</a>
            </div>
            <div class="col-md-12">

                @if ($message = Session::get('success'))
                    <div class="alert alert-success alert-block mt-20">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong> {{ $message }} </strong>
                    </div>
                @endif

                <div class="card">
                    <div class="card-header bg-success text-white">Alumnos</div>

                    <div class="card-body">
                        <div class="table-wrapper">
                            <table id="datatable1" class="table display responsive nowrap">
                                <thead>
                                    <tr>
                                        <th class="wd-15p">Documento</th>
                                        <th class="wd-15p">Alumno</th>
                                        <th class="wd-15p">Correo</th>
                                        <th class="wd-20p"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($alumnos as $alumno)
                                        <tr>
                                            <td>{{ $alumno->documento }}</td>
                                            <td>{{ $alumno->nombres }}  {{ $alumno->apellidos }}</td>
                                            <td>{{ $alumno->correo }}</td>
                                            <td>
                                                <a href="{{ route('alumnos.edit', $alumno->id) }}" class="btn btn-info"> <i
                                                        class="fa fa-edit"></i> </a>
                                                @if ($alumno->status == 1)
                                                    <a href="{{ route('alumnos.desactivar', $alumno->id) }}"
                                                        class="btn btn-danger"> <i class="fa fa-ban"></i> </a>
                                                @else
                                                    <a href="{{ route('alumnos.activar', $alumno->id) }}"
                                                        class="btn btn-success"> <i class="fa fa-check"></i>
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div><!-- table-wrapper -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('js')
    <script>
        $('#datatable1').DataTable({
            responsive: true,
            language: {
                searchPlaceholder: 'Buscar',
                sSearch: '',
                lengthMenu: '_MENU_ Registros por Pagina',
            }
        });
    </script>
@endsection
