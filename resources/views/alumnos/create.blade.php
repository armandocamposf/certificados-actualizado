@extends('layouts.app')

@section('contenido')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12"></div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 mb-3">
                <a href="{{ route('alumnos.index') }}" class="btn btn-warning w-100"> <i class="fa-solid fa-backward"></i> Regresar a Alumnos</a>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header bg-success text-white">Crear Alumno</div>

                    <div class="card-body">
                        <form action="{{ route('alumnos.store') }}" method="post">
                            @csrf
                        <div class="row">
                            <label class="col-sm-4 form-control-label mt-3">Tipo de Documento: <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0 mt-3">
                              <select name="idTipoDocumento" class="form-control" id="">
                                <option value="">SELECCIONE</option>
                                <option value="1">DNI</option>
                                <option value="2">Otro</option>
                              </select>
                            </div>

                            <label class="col-sm-4 form-control-label mt-3">Nro. Documento: <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0 mt-3">
                              <input type="text" class="form-control" required name="documento" >
                            </div>

                            <label class="col-sm-4 form-control-label mt-3">Nombres: <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0 mt-3">
                              <input type="text" class="form-control" required name="nombres" placeholder="">
                            </div>

                            <label class="col-sm-4 form-control-label mt-3">Apellidos: <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0 mt-3">
                              <input type="text" class="form-control" required name="apellidos" placeholder="">
                            </div>

                            <label class="col-sm-4 form-control-label mt-3">Correo: <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0 mt-3">
                              <input type="email" class="form-control" required name="correo" placeholder="">
                            </div>

                            <label class="col-sm-4 form-control-label mt-3">Telefono: <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0 mt-3">
                              <input type="text" class="form-control" required name="telefono" placeholder="">
                            </div>


                            <div class="col-sm-12 mt-3">
                                <button class="btn btn-success w-100 mt-3"> Guardar </button>
                            </div>
                          </div><!-- row -->

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
