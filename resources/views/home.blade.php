@extends('layouts.app')

@section('contenido')
<div class="container">

    <div class="row row-sm">
        <div class="col-lg-3">
          <div class="card bg-success">
            <div id="rs1" class="wd-100p ht-100"></div>
            <div class="overlay-body pd-x-20 pd-t-20">
              <div class="d-flex justify-content-between bg-success ">
                <div>
                  <h6 class="tx-12 tx-uppercase tx-inverse tx-bold mg-b-5 text-white">Alumnos</h6>
                </div>
                <a href="" class="tx-gray-600 hover-info"><i class="fa fa-users text-white tx-30 lh-0"></i></a>
              </div><!-- d-flex -->
              <h2 class="mg-b-5 tx-inverse tx-lato text-white">{{$totalAlumnos}}</h2>
            </div>
          </div><!-- card -->
        </div><!-- col-4 -->

        <div class="col-lg-3">
          <div class="card">
            <div id="rs1" class="wd-100p ht-100"></div>
            <div class="overlay-body pd-x-20 pd-t-20 bg-primary">
              <div class="d-flex justify-content-between">
                <div>
                  <h6 class="tx-12 tx-uppercase tx-inverse tx-bold mg-b-5 text-white">Docentes</h6>
                </div>
                <a href="" class="tx-gray-600 hover-info"><i class="fa fa-user text-white tx-30 lh-0"></i></a>
              </div><!-- d-flex -->
              <h2 class="mg-b-5 tx-inverse tx-lato text-white">{{$totalDocentes}}</h2>
            </div>
          </div><!-- card -->
        </div><!-- col-4 -->

        <div class="col-lg-3">
            <div class="card">
              <div id="rs1" class="wd-100p ht-100"></div>
              <div class="overlay-body pd-x-20 pd-t-20 bg-secondary">
                <div class="d-flex justify-content-between">
                  <div>
                    <h6 class="tx-12 tx-uppercase tx-inverse tx-bold mg-b-5 text-white">Cursos</h6>
                  </div>
                  <a href="" class="tx-gray-600 hover-info"><i class="fa fa-database text-white tx-30 lh-0"></i></a>
                </div><!-- d-flex -->
                <h2 class="mg-b-5 tx-inverse tx-lato text-white">{{$totalCursos}}</h2>
              </div>
            </div><!-- card -->
          </div><!-- col-4 -->

          <div class="col-lg-3">
            <div class="card">
              <div id="rs1" class="wd-100p ht-100"></div>
              <div class="overlay-body pd-x-20 pd-t-20 bg-info">
                <div class="d-flex justify-content-between">
                  <div>
                    <h6 class="tx-12 tx-uppercase tx-inverse tx-bold mg-b-5 text-white">Matriculas</h6>
                  </div>
                  <a href="" class="tx-gray-600 hover-info"><i class="fa fa-certificate tx-30 text-white lh-0"></i></a>
                </div><!-- d-flex -->
                <h2 class="mg-b-5 tx-inverse tx-lato text-white">{{$totalMatriculas}}</h2>
              </div>
            </div><!-- card -->
          </div><!-- col-4 -->

      </div><!-- row -->

    <div class="row justify-content-center mt-3">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Inicio') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    Bienvenido!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
