@extends('layouts.app')

@section('contenido')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12"></div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 mb-3">
                <a href="{{ route('matriculas.index') }}" class="btn btn-warning w-100"> <i class="fa-solid fa-backward"></i> Regresar a Matriculas</a>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header bg-success text-white">Crear Alumno</div>

                    <div class="card-body">

                        <form action="{{ route('matriculas.store') }}" method="post">
                            @csrf
                        <div class="row">
                            <label class="col-sm-2 form-control-label mt-3">Alumno: <span class="tx-danger">*</span></label>
                            <div class="col-sm-10 mg-t-10 mg-sm-t-0 mt-3">
                              <select name="idAlumno" class="form-control js-example-basic-single1" id="">
                                <option value="">SELECCIONE</option>
                                @foreach ($alumnos as $alumno)
                                    <option value="{{ $alumno->id }}">{{ $alumno->documento }} - {{ $alumno->nombres }} {{ $alumno->apellidos }}</option>
                                @endforeach
                              </select>
                            </div>

                            <label class="col-sm-2 form-control-label mt-3">Curso: <span class="tx-danger">*</span></label>
                            <div class="col-sm-10 mg-t-10 mg-sm-t-0 mt-3">
                              <select name="idCurso" class="form-control js-example-basic-single2" id="">
                                <option value="">SELECCIONE</option>
                                @foreach ($cursos as $curso)
                                    <option value="{{ $curso->id }}">{{ $curso->titulo }}</option>
                                @endforeach
                              </select>
                            </div>

                            <div class="col-sm-12 mt-3">
                                <button class="btn btn-success w-100 mt-3"> Guardar </button>
                            </div>
                          </div><!-- row -->

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script>
$(document).ready(function() {
    $('.js-example-basic-single1').select2();
    $('.js-example-basic-single2').select2();
});
</script>
@endsection
