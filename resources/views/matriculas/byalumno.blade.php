@extends('layouts.app')

@section('contenido')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12"></div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 mb-3">
                <a href="{{ route('matriculas.index') }}" class="btn btn-warning w-100"> <i class="fa-solid fa-backward"></i>
                    Regresar a Matriculas</a>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header bg-success text-white">Crear Alumno</div>

                    <div class="card-body">
                        <div class="row">
                            <label class="col-sm-2 form-control-label mt-3">Alumno: <span class="tx-danger">*</span></label>
                            <div class="col-sm-10 mg-t-10 mg-sm-t-0 mt-3">
                                <select name="idAlumno" id="idAlumno" class="form-control js-example-basic-single1"
                                    onchange="buscarMatriculas()" id="">
                                    <option value="">SELECCIONE</option>
                                    @foreach ($alumnos as $alumno)
                                        <option value="{{ $alumno->id }}">{{ $alumno->documento }} -
                                            {{ $alumno->nombres }} {{ $alumno->apellidos }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div><!-- row -->
                    </div>
                </div>


                <div class="card mt-3" style="display: none" id="cardData">
                    <div class="card-header bg-success text-white">Matriculas del Alumno</div>
                    <div class="card-body">
                        <div class="table-wrapper">
                            <table id="datatable1" class="table display responsive nowrap">
                                <thead>
                                    <tr>
                                        <th class="wd-15p">Curso</th>
                                        <th class="wd-20p"></th>
                                    </tr>
                                </thead>
                                <tbody id="DataCursos">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection

@section('js')
    <script>
        $(document).ready(function() {
            $('.js-example-basic-single1').select2();
            $('.js-example-basic-single2').select2();
        });

        function buscarMatriculas() {
            $("#cardData").css('display', 'block')

            const id = $("#idAlumno").val()

            axios.get('/api/matriculas/alumno/'+id).then((data) => {
                console.log(data.data)
                $("#DataCursos").html(data.data)
            })
        }
    </script>
@endsection
