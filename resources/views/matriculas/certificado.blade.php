<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <title>SSOMACAL | Buscador Certificados</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">

    <!-- Favicons -->
    <link href="{{ asset('icono.png') }}" rel="icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700"
        rel="stylesheet">

    <!-- Bootstrap CSS File -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"/>

    <!-- Libraries CSS Files -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css"
        integrity="sha512-SzlrxWUlpfuzQ+pcUCosxcglQRNAq/DZjVsC0lE40xsADsfeQoEypE+enwcOiGjk/bSuGGKHEyjSoQ1zVisanQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="lightbox.min.css" rel="stylesheet">

    <link href="{{ asset('style.css') }}" rel="stylesheet">
    <style>
        .size {
            width: 330px;
        }

        .size2 {
            width: 200px;
        }

        .azul{
            color: #001C7D!important;
        }

        .btn-azul{
            background-color: #001C7D!important;
        }

        .bg-azul{
            background-color: #001C7D!important;
            color: white;
        }

        .bg-mostaza{
            color: white;
            background-color: #BD9D12;
            border: solid 0px;
        }

        .bg-mostaza:hover{
            background-color: #CE9E1D;
            border: solid 0px;
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="row justify-content-md-right mt-3">
            <div class="col-md-8"></div>
            <div class="col-md-4">
                <a href="/" class="btn btn-success bg-mostaza w-100">Regresar</a>
            </div>

        </div>
        <div class="card mt-3">
            <div class="card-header bg-azul text-white">
                Datos del Alumnmo
            </div>
            <div class="card-body">
                <div class="list-group">
                    <div class="list-group-item">
                        <div class="row">
                            <div class="col-md-4">
                                Documento
                            </div>
                            <div class="col-md-8">
                                @if($matricula->alumno->idTipoDocumento == 1) DNI
                                @else Otro
                                @endif
                                - {{ $matricula->alumno->documento }}
                            </div>
                        </div>
                    </div>

                    <div class="list-group-item">
                        <div class="row">
                            <div class="col-md-4">
                                Nombres
                            </div>
                            <div class="col-md-8">
                                {{$matricula->alumno->nombres}}
                            </div>
                        </div>
                    </div>

                    <div class="list-group-item">
                        <div class="row">
                            <div class="col-md-4">
                                Apellidos
                            </div>
                            <div class="col-md-8">
                                {{$matricula->alumno->apellidos}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card mt-3">
            <div class="card-header bg-azul text-white">
                Datos del Docente
            </div>
            <div class="card-body">
                <div class="list-group">
                    <div class="list-group-item">
                        <div class="row">
                            <div class="col-md-4">
                                Documento
                            </div>
                            <div class="col-md-8">
                                @if($matricula->curso->profesor->idTipoDocumento == 1) DNI
                                @else Otro
                                @endif
                                - {{ $matricula->curso->profesor->documento }}
                            </div>
                        </div>
                    </div>

                    <div class="list-group-item">
                        <div class="row">
                            <div class="col-md-4">
                                Nombre
                            </div>
                            <div class="col-md-8">
                                {{$matricula->curso->profesor->nombres}}
                            </div>
                        </div>
                    </div>

                    <div class="list-group-item">
                        <div class="row">
                            <div class="col-md-4">
                                Profesion
                            </div>
                            <div class="col-md-8">
                                {{$matricula->curso->profesor->profesion}}
                            </div>
                        </div>
                    </div>

                    <div class="list-group-item">
                        <div class="row">
                            <div class="col-md-4">
                                Correo
                            </div>
                            <div class="col-md-8">
                                {{$matricula->curso->profesor->correo}}
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="card mt-3">
            <div class="card-header bg-azul text-white">
                Datos del Curso
            </div>
            <div class="card-body">
                <div class="list-group">
                    <div class="list-group-item">
                        <div class="row">
                            <div class="col-md-4">
                                Curso
                            </div>
                            <div class="col-md-8">
                                {{ $matricula->curso->titulo }}
                            </div>
                        </div>
                    </div>

                    <div class="list-group-item">
                        <div class="row">
                            <div class="col-md-4">
                                Descripcion
                            </div>
                            <div class="col-md-8">
                                {!! $matricula->curso->descripcion !!}
                            </div>
                        </div>
                    </div>

                    <div class="list-group-item">
                        <div class="row">
                            <div class="col-md-4">
                                Duracion:
                            </div>
                            <div class="col-md-8">
                                {{ $matricula->curso->duracion }} -
                                @if($matricula->curso->typeDuracion == 1) Horas Pedagogicas
                                @else Horas Academicas
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="list-group-item">
                        <div class="row">
                            <div class="col-md-4">
                                Fecha Emision:
                            </div>
                            <div class="col-md-8">
                                {{$matricula->curso->fechaEmision}}
                            </div>
                        </div>
                    </div>

                    <div class="list-group-item">
                        <div class="row">
                            <div class="col-md-4">
                                Fecha Inicio:
                            </div>
                            <div class="col-md-8">
                                {{$matricula->curso->fechaInicio}}
                            </div>
                        </div>
                    </div>

                    <div class="list-group-item">
                        <div class="row">
                            <div class="col-md-4">
                                Fecha Fin:
                            </div>
                            <div class="col-md-8">
                                {{$matricula->curso->fechaFin}}
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="card mt-3">
            <div class="card-header bg-azul text-white">
                Datos del Certificado
            </div>
            <div class="card-body">
                <div class="list-group">
                    <div class="list-group-item">
                        <div class="row">
                            <div class="col-md-4">
                                Codigo Verificador
                            </div>
                            <div class="col-md-8">
                                {{ $matricula->uuid }}
                            </div>
                        </div>
                    </div>

                    <div class="list-group-item">
                        <div class="row">
                            <div class="col-md-4">
                                Fecha de Asignacion
                            </div>
                            <div class="col-md-8">
                                {{ $matricula->created_at }}
                            </div>
                        </div>
                    </div>

                    <div class="list-group-item">
                        <div class="row">
                            <div class="col-md-4">
                                QR Verificacion
                            </div>
                            <div class="col-md-8">
                                {!! QrCode::size(300)->generate('https://6562-2800-200-e260-2e9-e43e-b652-e350-9c50.sa.ngrok.io/ver-certificado/'.$matricula->uuid) !!}
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>




        <div class="row justify-content-md-center mt-5 mb-5">
            <a href="{{ route('descargar-certificado', $matricula->uuid) }}" class="btn btn-success bg-mostaza"
            id="qrbuttom"><i class="fa fa-file"></i> Descargar</a>
        </div>
    </div>

    <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
    <script src="https://code.jquery.com/jquery-3.6.3.min.js"
        integrity="sha256-pvPw+upLPUjgMXY0G+8O0xUf+/Im1MZjXxxgOcBQBXU=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/1.3.2/axios.min.js"
        integrity="sha512-NCiXRSV460cHD9ClGDrTbTaw0muWUBf/zB/yLzJavRsPNUl9ODkUVmUHsZtKu17XknhsGlmyVoJxLg/ZQQEeGA=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
</body>

</html>
