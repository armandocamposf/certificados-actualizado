<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <title>SSOMACAL | Buscador Certificados</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">

    <!-- Favicons -->
    <link href="icono.png" rel="icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700"
        rel="stylesheet">

    <!-- Bootstrap CSS File -->
    <link href="bootstrap.min.css" rel="stylesheet">

    <!-- Libraries CSS Files -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css"
        integrity="sha512-SzlrxWUlpfuzQ+pcUCosxcglQRNAq/DZjVsC0lE40xsADsfeQoEypE+enwcOiGjk/bSuGGKHEyjSoQ1zVisanQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="lightbox.min.css" rel="stylesheet">

    <link href="style.css" rel="stylesheet">
    <style>
        .size {
            width: 330px;
        }

        .size2 {
            width: 200px;
        }
    </style>
</head>

<body>

    <header id="header" style="margin-bottom: 10rem">
        <div class="container-fluid">

            <div id="logo" class="pull-left">
                <h1><a href="/" class="border-0">
                        <img class="img-fluid" src="icono.png" style="width: 60px;"> </a></h1>
            </div>

            <nav id="nav-menu-container">
                <ul class="nav-menu">
                    <li class="menu-active"><a href="/">Buscador de Certificados</a></li>
                    <li class="menu-active"><a href="/">Regresar</a></li>
                </ul>
            </nav>
        </div>
    </header>

    <div class="container text-center" style="margin-top: 10rem!important">
        <h1>VALIDAR CERTIFICADO</h1>
        <div class="row justify-content-md-center">
        <div class="form-group col-8">
            <form action="{{ route('mostrar-certificado') }}" method="post">
                @csrf
            <div class="input-group mb-3">
                <input type="text" id="doc" name="matricula" class="form-control"
                    id="doc" placeholder="Codigo Certificado" autocomplete="off">
                <div class="input-group-append">
                    <button type="submit" class="btn btn-success"
                        id="qrbuttom"><i class="fa fa-search"></i> Buscar</button>
                </div>
            </form>
            </div>
        </div>
        </div>
    </div>

    <div class="carousel-item active">
        <div class="carousel-container" style="align-items: end; margin-top: 100px;">
            <div class="carousel-content">


                <div class="form-row justify-content-center" id="buscadornone">

                </div>
            </div>
        </div>
    </div>


    <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
    <script src="https://code.jquery.com/jquery-3.6.3.min.js"
        integrity="sha256-pvPw+upLPUjgMXY0G+8O0xUf+/Im1MZjXxxgOcBQBXU=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/1.3.2/axios.min.js"
        integrity="sha512-NCiXRSV460cHD9ClGDrTbTaw0muWUBf/zB/yLzJavRsPNUl9ODkUVmUHsZtKu17XknhsGlmyVoJxLg/ZQQEeGA=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <script>

    </script>

</body>

</html>
