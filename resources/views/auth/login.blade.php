<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <title>Iniciar Sesion</title>

    <!-- vendor css -->
    <link href="{{ asset('lib/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/Ionicons/css/ionicons.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/perfect-scrollbar/css/perfect-scrollbar.css') }}" rel="stylesheet">

    <!-- Amanda CSS -->
    <link rel="stylesheet" href="{{ asset('css/amanda.css') }}">
</head>

<body>

    <div class="am-signin-wrapper">
        <div class="am-signin-box">
            <div class="row no-gutters">
                <div class="col-lg-5">
                    <div>
                        <h2>Certificados - 2023</h2>
                    </div>
                </div>
                <div class="col-lg-7">
                    <form action="{{ route('login') }}" method="post">
                        @csrf
                        <h5 class="tx-gray-800 mg-b-25">Inicie Sesion en su cuenta</h5>

                        <div class="form-group">
                            <label class="form-control-label">Usuario:</label>
                            <input type="text" class="form-control" name="email" value="{{ old('email') }}"
                                placeholder="">
                        </div><!-- form-group -->

                        <div class="form-group">
                            <label class="form-control-label">Clave:</label>
                            <input type="password" name="password" class="form-control" placeholder="******">
                        </div>

                        <button type="submit" class="btn btn-block">Iniciar Sesion</button>
                    </form>
                </div><!-- col-7 -->
            </div><!-- row -->
        </div><!-- signin-box -->
    </div><!-- am-signin-wrapper -->

    <script src="{{ asset('lib/jquery/jquery.js') }}"></script>
    <script src="{{ asset('lib/popper.js/popper.js') }}"></script>
    <script src="{{ asset('lib/bootstrap/bootstrap.js') }}"></script>
    <script src="{{ asset('lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js') }}"></script>

    <script src="{{ asset('js/amanda.js') }}"></script>
</body>

</html>
