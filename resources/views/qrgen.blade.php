<!DOCTYPE html>
<html><head>
    <meta charset="utf-8">
    <title>How to Generate QR Code in Laravel 9</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"/>
</head><body>    <div class="container mt-4">        <div class="card">
            <div class="card-header">
                <h2>Qr Validacion del certificado</h2>
            </div>
            <div class="card-body">
                {!! QrCode::size(300)->generate('https://6562-2800-200-e260-2e9-e43e-b652-e350-9c50.sa.ngrok.io/ver-certificado/'.$matricula) !!}
            </div>
        </div>         </div>
</body>
</html>