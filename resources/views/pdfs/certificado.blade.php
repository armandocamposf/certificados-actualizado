<style>
    @page {
        margin-left: 0.1cm;
        margin-right: 0.1cm;
        margin-top: 0.1cm;
        margin-bottom: 0.1cm;
    }

    .contenedor {
        text-align: center;
        margin-top: 2rem
    }

    .titulo {
        font-size: 4rem;
        font-weight: 700;
        margin-top: 6rem !important;
    }

    .otorgado {
        font-size: 2rem;
        font-weight: 400;
    }

    .nombre {
        font-size: 2rem;
        font-weight: 800;
        margin-top: 2rem
    }

    .documento {
        font-size: 1rem;
        margin-top: 0.5rem
    }

    .asistido {
        margin-top: 2rem
    }

    .titulo-curso {
        color: #B8860B;
        margin-top: 1rem;
        font-size: 2rem;
        font-weight: 500
    }

    .uuid {
        transform: rotate(90deg);
        position: absolute;
        bottom: 210;
        right: -90;
    }

    .qrcode {
        position: absolute;
        bottom: 20;
        left: 70;
    }
</style>

<body
    style="background-image:url({{ $imagen }}); background-repeat: no-repeat; background-position: center top; background-size: cover; text-aling: center;">
    <div class="contenedor">
        <div class="titulo">
            {{ $matricula->curso->lineas->titulo }}
        </div>

        <div class="otorgado">
            {{ $matricula->curso->lineas->otorgado }}
        </div>

        <div class="nombre">
            {{ $matricula->alumno->nombres }} {{ $matricula->alumno->apellidos }}
        </div>

        <div class="documento">
            DNI - {{ $matricula->alumno->documento }}
        </div>

        <div class="asistido">
            {{ $matricula->curso->lineas->descrip1 }}
        </div>

        <div class="titulo-curso">
            {{ strtoupper($matricula->curso->titulo) }}
        </div>

        <div class="fecha-curso">
            Certificado emitido a los {{ $fecha->day }} dias del mes de
            @if ($fecha->format('m') == 01)
                Enero
            @elseif($fecha->format('m') == 02)
                Febrero
            @elseif($fecha->format('m') == 03)
                Marzo
            @elseif($fecha->format('m') == 04)
                Abril
            @elseif($fecha->format('m') == 05)
                Mayo
            @elseif($fecha->format('m') == 06)
                Junio
            @elseif($fecha->format('m') == 07)
                Julio
            @elseif($fecha->format('m') == '08')
                Agosto
            @elseif($fecha->format('m') == '09')
                Septiembre
            @elseif($fecha->format('m') == '10')
                Octubre
            @elseif($fecha->format('m') == '11')
                Noviembre
            @else
                Diciembre
            @endif

            del año {{ $fecha->format('Y') }}. <br> Con un total de {{ $matricula->curso->duracion }} de horas
            @if ($matricula->curso->typeDuracion == 1)
                Horas Pedagogicas
            @else
                Horas Academicas
            @endif
        </div>

        <div class="uuid">
            ID. {{ $matricula->uuid }}
        </div>

        <div class="qrcode">
            <img src="data:image/png;base64, {!! $qrcode !!}">
        </div>
    </div>
</body>
