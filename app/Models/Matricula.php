<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Matricula extends Model
{
    use HasFactory;

    protected $fillable = [
        'uuid',
        'idCurso',
        'idAlumno'
    ];

    public function curso()
    {
        return $this->hasOne(Curso::class, 'id', 'idCurso');
    }

    public function alumno()
    {
        return $this->hasOne(Alumno::class, 'id', 'idAlumno');
    }

}
