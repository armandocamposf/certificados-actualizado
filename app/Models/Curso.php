<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Curso extends Model
{
    use HasFactory;

    protected $fillable = [
        'titulo',
        'duracion',
        'typeDuracion',
        'fechaEmision',
        'fechaInicio',
        'fechaFin',
        'idDocente',
        'modelo_certificado',
        'descripcion'
    ];

    public function profesor()
    {
        return $this->hasOne(Docente::class, 'id', 'idDocente');
    }

    public function lineas()
    {
        return $this->hasOne(LineasCertificado::class, 'idCurso', 'id');
    }
}
