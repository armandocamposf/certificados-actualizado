<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use App\Models\Curso;
use App\Models\Docente;
use App\Models\LineasCertificado;
use Illuminate\Http\Request;

class CursoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $cursos = Curso::with('profesor')->get();
        return view('cursos.index', compact('cursos'));
    }

    public function create()
    {
        $docentes = Docente::whereStatus(1)->get();
        return view('cursos.create', compact('docentes'));
    }
    public function store(Request $request)
    {

        $curso = Curso::create([
            'titulo' => $request->titulo,
            'duracion' => $request->duracion,
            'typeDuracion' => $request->tipo_duracion,
            'fechaEmision' => $request->fechaEmision,
            'fechaInicio' => $request->fechaInicio,
            'fechaFin' => $request->fechaFin,
            'idDocente' => $request->idDocente,
            'modelo_certificado' => 0,
            'descripcion' => $request->descripcion
        ]);

        $textos = LineasCertificado::create([
            'idCurso' => $curso->id
        ]);

        return redirect()->route('cursos.index')->with('success', 'Curso Registrado con exito');
    }

    public function edit(Curso $curso)
    {
        $docentes = Docente::whereStatus(1)->get();
        return view('cursos.edit', compact('curso', 'docentes'));
    }

    public function update(Request $request, Curso $curso)
    {
        $textos = LineasCertificado::where('idCurso', $curso->id)->first();

        if($textos == null)
        {
            $textos = LineasCertificado::create([
                'idCurso' => $curso->id
            ]);
        }

        $curso->titulo = $request->titulo;
        $curso->duracion = $request->duracion;
        $curso->typeDuracion = $request->tipo_duracion;
        $curso->fechaEmision = $request->fechaEmision;
        $curso->fechaInicio = $request->fechaInicio;
        $curso->fechaFin = $request->fechaFin;
        $curso->idDocente = $request->idDocente;
        $curso->descripcion = $request->descripcion;
        $curso->save();

        return redirect()->route('cursos.index')->with('success', 'Curso '. $curso->titulo .' Actualizado con exito');
    }

    public function desactivar(Curso $curso)
    {
        $curso->status = 2;
        $curso->save();

        return redirect()->route('cursos.index')->with('success', 'Curso '. $curso->titulo .' desactivado con exito');
    }

    public function activar(Curso $curso)
    {
        $curso->status = 1;
        $curso->save();

        return redirect()->route('cursos.index')->with('success', 'Curso '. $curso->titulo .' activado con exito');
    }

    public function carga(Curso $curso)
    {
        return view('cursos.cargar', compact('curso'));
    }

    public function upload(Request $request, Curso $curso)
    {
        // script para subir la imagen
        if($request->hasFile("fondo")){

            $imagen = $request->file("fondo");
            $nombreimagen = date('Ymdshis').".".$imagen->guessExtension();
            $ruta = public_path("img/certificados/");

            //$imagen->move($ruta,$nombreimagen);
            copy($imagen->getRealPath(),$ruta.$nombreimagen);
            $curso->modelo_certificado = $nombreimagen;
        }
        $curso->save();

        return redirect()->route('cursos.index')->with('success', 'Curso '. $curso->titulo .' has actualizado su formato de certificado');
    }
}
