<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreLineasCertificadoRequest;
use App\Http\Requests\UpdateLineasCertificadoRequest;
use App\Models\LineasCertificado;

class LineasCertificadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreLineasCertificadoRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreLineasCertificadoRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\LineasCertificado  $lineasCertificado
     * @return \Illuminate\Http\Response
     */
    public function show(LineasCertificado $lineasCertificado)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\LineasCertificado  $lineasCertificado
     * @return \Illuminate\Http\Response
     */
    public function edit(LineasCertificado $lineasCertificado)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateLineasCertificadoRequest  $request
     * @param  \App\Models\LineasCertificado  $lineasCertificado
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateLineasCertificadoRequest $request, LineasCertificado $lineasCertificado)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\LineasCertificado  $lineasCertificado
     * @return \Illuminate\Http\Response
     */
    public function destroy(LineasCertificado $lineasCertificado)
    {
        //
    }
}
