<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreDocenteRequest;
use App\Http\Requests\UpdateDocenteRequest;
use App\Models\Docente;
use Illuminate\Http\Request;

class DocenteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $docentes = Docente::all();

        return view('docentes.index', compact('docentes'));
    }

    public function create()
    {
        return view('docentes.create');
    }

    public function store(Request $request)
    {
        $docente = Docente::create([
            'idTipoDocumento' => $request->idTipoDocumento,
            'documento' => $request->documento,
            'nombres' => $request->nombres,
            'profesion' => $request->profesion,
            'correo' => $request->correo,
        ]);

        return redirect()->route('docentes.index')->with('success', 'Curso '. $docente->nombres .' Creado con exito');
    }


    public function edit(Docente $docente)
    {
        return view('docentes.edit', compact('docente'));
    }

    public function update(Request $request, Docente $docente)
    {
        $docente->idTipoDocumento = $request->idTipoDocumento;
        $docente->documento = $request->documento;
        $docente->nombres = $request->nombres;
        $docente->profesion = $request->profesion;
        $docente->correo = $request->correo;

        $docente->save();
        return redirect()->route('docentes.index')->with('success', 'Curso '. $docente->nombres .' actualizado con exito');
    }

    public function activar(Docente $docente)
    {
        $docente->status = 1;
        $docente->save();

        return redirect()->route('docentes.index')->with('success', 'Curso '. $docente->nombres .' activado con exito');
    }

    public function desactivar(Docente $docente)
    {
        $docente->status = 2;
        $docente->save();

        return redirect()->route('docentes.index')->with('success', 'Curso '. $docente->nombres .' desactivado con exito');
    }

    public function firma(Docente $docente)
    {
        return view('docentes.cargar', compact('docente'));
    }

    public function uploadImage(Request $request, Docente $docente)
    {
        if($request->hasFile("fondo")){

            $imagen = $request->file("fondo");
            $nombreimagen = date('Ymdshis').".".$imagen->guessExtension();
            $ruta = public_path("img/firma_docente/");

            //$imagen->move($ruta,$nombreimagen);
            copy($imagen->getRealPath(),$ruta.$nombreimagen);
            $docente->firma = $nombreimagen;
        }
        $docente->save();

        return redirect()->route('cursos.index')->with('success', 'Has cargado la firma del docente: '. $docente->nombres );
    }
}
