<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreAlumnoRequest;
use App\Http\Requests\UpdateAlumnoRequest;
use App\Models\Alumno;
use Illuminate\Http\Request;

class AlumnoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $alumnos = Alumno::all();

        return view('alumnos.index', compact('alumnos'));
    }

    public function create()
    {
        return view('alumnos.create');
    }

    public function store(Request $request)
    {
        $alumno = Alumno::create([
            'idTipoDocumento' => $request->idTipoDocumento,
            'documento' => $request->documento,
            'nombres' => $request->nombres,
            'apellidos' => $request->apellidos,
            'correo' => $request->correo,
            'telefono' => $request->telefono,
        ]);

        return redirect()->route('alumnos.index')->with('success', 'Alumno creado con exito');
    }

    public function edit(Alumno $alumno)
    {
        return view('alumnos.edit', compact('alumno'));
    }

    public function update(Request $request, Alumno $alumno)
    {
        $alumno->idTipoDocumento = $request->idTipoDocumento;
        $alumno->documento = $request->documento;
        $alumno->nombres = $request->nombres;
        $alumno->apellidos = $request->apellidos;
        $alumno->correo = $request->correo;
        $alumno->telefono = $request->telefono;
        $alumno->save();
        return redirect()->route('alumnos.index')->with('success', 'Alumno '. $alumno->nombres .' editado con exito');
    }

    public function activar(Alumno $alumno)
    {
        $alumno->status = 1;
        $alumno->save();

        return redirect()->route('alumnos.index')->with('success', 'Alumno '. $alumno->nombres .' activado con exito');
    }

    public function desactivar(Alumno $alumno)
    {
        $alumno->status = 2;
        $alumno->save();

        return redirect()->route('alumnos.index')->with('success', 'Alumno '. $alumno->nombres .' desactivado con exito');
    }
}
