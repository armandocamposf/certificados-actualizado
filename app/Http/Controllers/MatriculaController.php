<?php

namespace App\Http\Controllers;

use App\Models\Alumno;
use App\Models\Curso;
use App\Models\Docente;
use App\Models\Matricula;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Barryvdh\DomPDF\Facade\Pdf;
use Carbon\Carbon;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class MatriculaController extends Controller
{
    public function __construct()
    {
    }
    public function index()
    {
        $matriculas = Matricula::with('curso', 'alumno')->get();
        return view('matriculas.index', compact('matriculas'));
    }

    public function create()
    {
        $alumnos = Alumno::whereStatus(1)->get();
        $cursos = Curso::whereStatus(1)->get();
        return view('matriculas.create', compact('alumnos', 'cursos'));
    }
    public function store(Request $request)
    {
        $mat = Matricula::where('idAlumno', $request->idAlumno)->where('idCurso', $request->idCurso)->count();

        if($mat > 0)
        {
            return redirect()->route('matriculas.index')->with('success', 'Matricula ya existe correctamente');
        }
        else
        {
            $matricula = Matricula::create([
            'uuid' => Str::uuid()->toString(),
            'idCurso' => $request->idCurso,
            'idAlumno' => $request->idAlumno,
        ]);

        return redirect()->route('matriculas.index')->with('success', 'Matricula agregada correctamente');
        }

    }

    public function destroy(Matricula $matricula)
    {
        $matricula->delete();

        return redirect()->route('matriculas.index')->with('success', 'Matricula eliminada correctamente');
    }

    public function byAlumno()
    {
        $alumnos = Alumno::All();

        return view('matriculas.byalumno', compact('alumnos'));
    }

    public function dataAlumno($alumno)
    {
        $matriculas = Matricula::where('idAlumno', $alumno)->with('curso')->get();

        if($matriculas->count() == 0)
        {
            return "NO existen matriculas para este estudiante";
        }

        $html = "";

        foreach ($matriculas as $matricula ) {
            $html .= "<tr>";
            $html .= "<td>".$matricula->curso->titulo."</td>";
            $html .= "<td><a href='/ver-certificado/$matricula->uuid' target='_blank' class='btn btn-success text-white'><i class='fa fa-eye'></i></a></td>";
            $html .= "</tr>";
        }

        return $html;
    }

    public function dataGeneral($alumno)
    {
        $alumno = Alumno::whereDocumento($alumno)->first();

        if($alumno == null)
        {
            return response()->json([
                'error' => 404,
                'message' => "No existe ningun alumno en nuestra base de datos con ese numero de documento, intente nuevamente"
            ]);
        }

        $matriculas = Matricula::where('idAlumno', $alumno->id)->with('curso')->get();
        $html = "";

        foreach($matriculas as $matricula)
        {
            $html .= "<tr>";
            $html .= "<td>".$matricula->uuid."</td>";
            $html .= "<td>".$matricula->curso->titulo."</td>";
            $html .= "<td>" . $matricula->curso->fechaInicio . "</td>";
            $html .= "<td>" . $matricula->curso->fechaFin . "</td>";
            $html .= "<td>" . $matricula->curso->fechaEmision . "</td>";
            $html .= "<td><a href='/descargar-certificado/$matricula->uuid' class='btn btn-success bg-azul' target='_blank'><i class='fa fa-file text-white'></i></a>
            <a href='/ver-certificado/$matricula->uuid' class='btn btn-warning bg-mostaza' target='_blank'><i class='fa fa-eye text-white'></i></a>
            </td>";
            $html .= "</tr>";
        }

        return response()->json([
            'alumno' => $alumno,
            'matriculas' => $matriculas,
            'html' => $html
        ]);
    }

    public function vericador()
    {
        return view('matriculas.verificador');
    }

    public function mostrar(Request $request)
    {
        $matricula = Matricula::whereUuid($request->matricula)->with('alumno', 'curso', 'curso.profesor')->first();

        return view('matriculas.certificado', compact('matricula'));
    }

    public function mostrar2($matricula)
    {
        $matricula = Matricula::whereUuid($matricula)->with('alumno', 'curso', 'curso.profesor')->first();

        return view('matriculas.certificado', compact('matricula'));
    }

    public function createQr($matricula)
    {
        return view('qrgen', compact('matricula'));
    }

    public function emitirCertificado($matricula)
    {
        $matricula = Matricula::whereUuid($matricula)->with('alumno', 'curso', 'curso.profesor', 'curso.lineas')->first();
        $qrcode = base64_encode(QrCode::format('svg')->size(100)->generate('https://a97b-179-6-18-128.ngrok.io/ver-certificado/'.$matricula->uuid));
        $imagen = public_path('/img/certificados/' . $matricula->curso->modelo_certificado);
        $fecha = Carbon::parse($matricula->curso->fechaEmision);
        $pdf = Pdf::loadView('pdfs.certificado', compact('matricula', 'qrcode', 'imagen', 'fecha'))->setPaper('a4', 'landscape');
        return $pdf->stream('certificado_'.$matricula->uuid.'.pdf');
    }

    public function descargar($matricula)
    {
        $matricula = Matricula::whereUuid($matricula)->with('alumno', 'curso', 'curso.profesor', 'curso.lineas')->first();
        $qrcode = base64_encode(QrCode::format('svg')->size(100)->generate('https://a97b-179-6-18-128.ngrok.io/ver-certificado/'.$matricula->uuid));
        $imagen = public_path('/img/certificados/' . $matricula->curso->modelo_certificado);
        $fecha = Carbon::parse($matricula->curso->fechaEmision);
        $pdf = Pdf::loadView('pdfs.certificado', compact('matricula', 'qrcode', 'imagen', 'fecha'))->setPaper('a4', 'landscape');
        return $pdf->download('certificado_'.$matricula->uuid.'.pdf');
    }
}
